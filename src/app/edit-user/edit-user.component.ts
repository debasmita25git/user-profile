import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  item:any;
  obj: any;
  name: any;

  constructor(private activatedRoute: ActivatedRoute) {
    this.item = this.activatedRoute.snapshot.paramMap.get('item');
    this.obj = JSON.parse(this.item);
   }

  ngOnInit(): void {
  }

  profileForm = new FormGroup(
    {
      name : new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl(null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")])
    }
  )

  onSubmit(){
    console.log('Edit Value: ', this.profileForm.value)
  }

}
