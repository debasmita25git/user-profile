import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userArr = [
    {_id: 1, name: 'Jon', email: 'jon@mail.com', phone: 9898767654, image: ''},
    {_id: 2, name: 'Archi', email: 'arch@mail.com', phone: 6767899878, image: ''},
    {_id: 3, name: 'David', email: 'david@mail.com', phone: 9898767654, image: ''},
    {_id: 4, name: 'Cane', email: 'cane@mail.com', phone: 6767899878, image: ''},
    {_id: 5, name: 'Marri', email: 'mari@mail.com', phone: 9898767654, image: ''},
    {_id: 6, name: 'Hena', email: 'hena@mail.com', phone: 6767899878, image: ''},
    {_id: 7, name: 'Charli', email: 'charli@mail.com', phone: 6767899878, image: ''}
  ]

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  goToEdit(item: any){
    let queryp = JSON.stringify(item);
    
    this.route.navigate(['/user-view', queryp])
  }

}
