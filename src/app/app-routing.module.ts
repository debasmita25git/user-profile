import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  {path: 'user-list', component: UserListComponent},
  {path: '', redirectTo:'/user-list', pathMatch:'full'},
  {path: 'user-add', component: UserAddComponent},
  {path: 'user-view/:item', component: EditUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
